//$("#create").addEventListener("click", () => {
//console.log("pressed button");

////console.log(data);
////var xmlhttp = new XMLHttpRequest();
////xmlhttp.open("GET", '/create', true);
////xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
////xmlhttp.onloadend = function(e){
////console.log(xmlhttp.responseText);
////}
////xmlhttp.send(JSON.stringify(data));
//var xhttp = new XMLHttpRequest();
//xhttp.onreadystatechange = function () {
//if (this.readyState == 4 && this.status == 200) {
//document.getElementById("common_name").innerHTML = 'dog'
//}
//};
//xhttp.open("GET", true);
//xhttp.send();
//});
$(function () {
  $("#change").on("submit", function (event) {
    event.preventDefault();

    let bits = $("#bits");
    let bit_value = bits.val();
    let common_name = $("#common_name").val();
    let days_exp = $("#days_of_expiration").val();
    let ISO = $("#ISO").val();
    let state = $("#state").val();
    let locality = $("#locality").val();
    let organization = $("#organization").val();
    let organization_unit = $("#organization_unit").val();
    let email = $("#email").val();
    let regen_key = $("#regenerate_key").is(":checked");

    let data2 = {
      bit: bit_value,
      common_name: common_name,
      days_exp: days_exp,
      ISO: ISO,
      state: state,
      locality: locality,
      organization: organization,
      organization_unit: organization_unit,
      email: email,
      regen_key: regen_key,
    };
    console.log(data2)
    $.ajax({
      url: "/create",
      method: "POST",
      contentType: "application/json",
      data: JSON.stringify(data2),
      //data: data2,
      success: function (res) {
        console.log(res.response);
        $("#download_btn").show()
        $("#download_link").attr("href", "./index.html")
      },
    });
  });
});
